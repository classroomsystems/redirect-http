# Redirect HTTP

Redirect HTTP is a very simple HTTP server
that redirects all incoming requests,
changing the URL scheme to HTTPS.

