package main

import (
	"flag"
	"log"
	"net/http"
	"net/url"
	"strings"
)

func main() {
	addr := flag.String("addr", ":8443", "listen address")
	flag.Parse()
	log.Fatal(http.ListenAndServe(*addr, http.HandlerFunc(handle)))
}

func handle(w http.ResponseWriter, r *http.Request) {
	var u url.URL = *r.URL
	u.Scheme = "https"
	u.Host = r.Host
	// Strip off any port specification.
	if i := strings.IndexByte(u.Host, ':'); i >= 0 {
		u.Host = u.Host[:i]
	}
	http.Redirect(w, r, u.String(), http.StatusMovedPermanently)
}
